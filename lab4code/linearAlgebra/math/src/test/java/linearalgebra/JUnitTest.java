package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class JUnitTest {
    @Test
    public void shouldReturn9()
    {
        Vector3d Vector1 = new Vector3d(2,3,5);
        double x=Vector1.getx();
        double y=Vector1.gety();
        double z=Vector1.getz();
        assertEquals(2,x,0.001);
        assertEquals(3,y,0.001);
        assertEquals(5,z,0.001);

    }
    
    @Test   
    public void magnetudeShouldReturn6()
    {
        Vector3d Vector1 = new Vector3d(2,3,5);
        double result=Vector1.magnitude();
        assertEquals(6.164414002968976,result,0.001);
    }
}
