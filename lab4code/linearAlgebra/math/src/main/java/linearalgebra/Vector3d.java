package linearalgebra;


/**
 * Hello world!
 *
 */
public class Vector3d {

    private double x;
    private double y;
    private double z;

    public Vector3d( double x,double y,double z)
    {
        this.x=x;
        this.y=y;
        this.z=z;
    }

    public double getx()
    {
        return this.x;
    }

    public double gety()
    {
        return this.y;
    }

    public double getz()
    {
        return this.z;
    }

    public double magnitude()
    {
        return Math.sqrt((x*x)+(y*y)+(z*z));
    }

    public double dotProduct(Vector3d vector)
    {
        double newx= vector.getx()*this.x;
        double newy= vector.gety()*this.y;
        double newz= vector.getz()*this.z;

        return newx+newy+newz;
    }

    public Vector3d add(Vector3d vector)
    {
        double v3x=vector.getx()+this.x;
        double v3y=vector.gety()+this.y;
        double v3z=vector.getz()+this.z;
        Vector3d Vector3 =new Vector3d(v3x,v3y,v3z);
        return Vector3;
    }

    
}
