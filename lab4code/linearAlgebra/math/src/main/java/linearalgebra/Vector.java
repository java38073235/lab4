package linearalgebra;

public class Vector {
    
    public static void main( String[] args )
    {
        
        Vector3d Vector1 =new Vector3d(2,3,5);
        Vector3d Vector2 =new Vector3d(3,4,6);

        System.out.println(Vector1.getx()+ Vector1.gety()+ Vector1.getz());
        double magResult=Vector1.magnitude();
        System.out.println(magResult);
        double dotResult=Vector1.dotProduct(Vector1);
        System.out.println(dotResult);
        Vector3d addResult=Vector1.add(Vector1);
        System.out.println(addResult.getx()+ addResult.gety()+ addResult.getz());
    }
    
}
